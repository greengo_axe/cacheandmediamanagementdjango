from django.urls import path
from django.views.decorators.cache import cache_page
from . import views

app_name = 'gallery'

urlpatterns = [
    path('',
         views.gallery_hp,
         name='homepage'),
    path('video',
         views.video_list,
         name='video_list'),

    path('audio',
         views.audio_list,
         name='audio_list'),

    path('image',
         views.image_list,
         name='image_list'),    # caching view 15 min

    path('<model_name>/create/',
         views.GalleryCreateUpdateView.as_view(),
         name='gallery_create'),

    # path('<model_name>/<int:id>/',
    #      views.GalleryCreateUpdateView.as_view(),
    #      name='gallery_update'),
]
