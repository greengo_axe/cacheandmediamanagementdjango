from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic.base import TemplateResponseMixin, View
from django.apps import apps
from django.forms.models import modelform_factory
from .models import Video, Audio, Image, Content
from .common import get_file_extension, delete_cache
from django.core.cache import cache
from var_dump import var_dump


def gallery_hp(request):
    return render(request,
                  'gallery/homepage.html',
                  )


# low-level cache API
def audio_list(request):
    audios = cache.get('audios')
    if not audios:
        audios = Audio.objects.all()
        cache.set('audios', audios)
    return render(request,
                  'gallery/audio/list.html',
                  {'audios': audios})


# low-level cache API
def image_list(request):
    images = cache.get('images')
    if not images:
        images = Image.objects.all()
        cache.set('images', images)
    return render(request,
                  'gallery/image/list.html',
                  {'images': images})


# low-level cache API
def video_list(request):
    videos = cache.get('videos')
    if not videos:
        videos = Video.objects.all()
        cache.set('videos', videos)
    return render(request,
                  'gallery/video/list.html',
                  {'videos': videos})


class GalleryCreateUpdateView(TemplateResponseMixin, View):
    model = None
    obj = None
    template_name = 'gallery/media_form.html'

    def get_model(self, model_name):
        if model_name in ['video', 'image', 'audio']:
            return apps.get_model(app_label='gallery',
                                  model_name=model_name)
        return None

    def get_form(self, model, *args, **kwargs):
        form = modelform_factory(model, exclude=['created', 'updated'])

        return form(*args, **kwargs)

    def dispatch(self, request, model_name, id=None):
        self.model = self.get_model(model_name)
        if id:
            self.obj = get_object_or_404(self.model,
                                         id=id)
        return super().dispatch(request, model_name, id)

    def get(self, request, model_name, id=None):
        form = self.get_form(self.model, instance=self.obj)
        return self.render_to_response({'form': form,
                                        'object': self.obj})

    def post(self, request, model_name, id=None):
        form = self.get_form(self.model,
                             instance=self.obj,
                             data=request.POST,
                             files=request.FILES)

        if form.is_valid():
            obj = form.save()
            fe = get_file_extension(model_name, form)
            # var_dump(fe)
            if fe.get('name') not in fe.get('list_allowed'):
                obj.delete()
                messages.error(request, 'File extension not allowed')
                return redirect(reverse('gallery:gallery_create', args=[model_name]))

            if not id:
                # new content
                Content.objects.create(item=obj)
                delete_cache(model_name)
                return redirect('gallery:homepage')

        return self.render_to_response({'form': form, 'object': self.obj})
