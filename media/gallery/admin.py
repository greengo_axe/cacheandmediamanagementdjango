from django.contrib import admin
from .models import Video, Audio, Image


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'file')
    list_filter = ('title', 'created', 'file')
    search_fields = ('title', 'created')
    date_hierarchy = 'created'


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    list_display = ('file',)
    list_filter = ('title', 'created', 'file')
    search_fields = ('title', 'created')
    date_hierarchy = 'created'


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('file',)
    list_filter = ('title', 'created', 'file')
    search_fields = ('title', 'created')
    date_hierarchy = 'created'


admin.site.index_template = 'memcache_status/admin_index.html'
