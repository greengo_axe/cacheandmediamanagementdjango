from django.core.cache import cache
import fleep
from var_dump import var_dump


def get_file_extension(model_name, form):
    list_allowed = []
    with open('media/' + model_name + 's/' + form.cleaned_data['file'].name, "rb") as file:
        file_info = fleep.get(file.read(128))

    if model_name == 'video':
        list_allowed = ['mp4', 'avi', 'mov']
    elif model_name == 'audio':
        list_allowed = ['mp3', 'ogg', 'wav', 'flac']
    elif model_name == 'image':
        list_allowed = ['jpg', 'jpeg', 'tiff', 'png']

    fe = {'name': file_info.extension[0], 'list_allowed': list_allowed}

    return fe


def delete_cache(model_name):
    var_dump(cache)
    if model_name == 'video':
        cache.delete('videos')
    elif model_name == 'audio':
        cache.delete('audios')
    elif model_name == 'image':
        cache.delete('images')
