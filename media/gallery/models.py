from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.validators import RegexValidator
from django.db.models.signals import post_delete
from django.dispatch import receiver


class Content(models.Model):
    content_type = models.ForeignKey(ContentType,
                                     on_delete=models.CASCADE,
                                     limit_choices_to={'model__in': (
                                        'video',
                                        'image',
                                        'audio')})
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ['-object_id']


class ItemBase(models.Model):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]+$', 'Only alphanumeric characters, are allowed.')

    title = models.CharField(max_length=250, validators=[alphanumeric])
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class Audio(ItemBase):
    file = models.FileField(upload_to='audios', blank=False)


class Image(ItemBase):
    file = models.FileField(upload_to='images', blank=False)


class Video(ItemBase):
    # url = RegexValidator(r'%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%', 'URL not allowed.')
    # url = models.URLField(blank=True, validators=[url])
    file = models.FileField(upload_to='videos', blank=False)


''' Credits : Michael Nelson, Mattias Omisore '''
""" Whenever ANY model is deleted, if it has a file field on it, delete the associated file too"""


@receiver(post_delete)
def delete_files_when_row_deleted_from_db(sender, instance, **kwargs):
    for field in sender._meta.concrete_fields:
        if isinstance(field, models.FileField):
            instance_file_field = getattr(instance, field.name)
            delete_file_if_unused(sender, instance, field, instance_file_field)


""" Delete the file if something else get uploaded in its place"""


# @receiver(pre_save)
# def delete_files_when_file_changed(sender, instance, **kwargs):
#     # Don't run on initial save
#     if not instance.pk:
#         return
#     for field in sender._meta.concrete_fields:
#         if isinstance(field, models.FileField):
#             # its got a file field. Let's see if it changed
#             try:
#                 instance_in_db = sender.objects.get(pk=instance.pk)
#             except sender.DoesNotExist:
#                 # We are probably in a transaction and the PK is just temporary
#                 # Don't worry about deleting attachments if they aren't actually saved yet.
#                 return
#             instance_in_db_file_field = getattr(instance_in_db, field.name)
#             instance_file_field = getattr(instance, field.name)
#             if instance_in_db_file_field.name != instance_file_field.name:
#                 delete_file_if_unused(sender, instance, field, instance_in_db_file_field)


""" Only delete the file if no other instances of that model are using it"""


def delete_file_if_unused(model, instance, field, instance_file_field):
    dynamic_field = {}
    dynamic_field[field.name] = instance_file_field.name
    other_refs_exist = model.objects.filter(**dynamic_field).exclude(pk=instance.pk).exists()
    if not other_refs_exist:
        instance_file_field.delete(False)


''' end credits '''
