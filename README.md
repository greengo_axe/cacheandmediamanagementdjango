# Media, Django and Memcached

## Abstract 
A simple media gallery with cache management. The [Antonio Mele's originary code](https://github.com/PacktPublishing/Django-3-by-Example)

### Features:
* File extension validation
* Deletion both entry and file related through admin panel
* Memcached : low-level API cache
* 1 single form (model/view) for every media

## Premise
This project requires :

* Python 3.8
* Memcached  (For all info about installation, click the following link:  [memcached.org](https://memcached.org))

They have to be up and running. 

## Installation
* Python Virtual Environment (venv) from a terminal (bash shell):

>> **mkdir /var/www/html/cacheandmediamanagementdjango**

>> **chmod -R 777 /var/www/html/cacheandmediamanagementdjango**

>> **cd /var/www/html/cacheandmediamanagementdjango**

>> **python -m venv venv**

>> **source venv/bin/activate**

>> **pip install "Django==3.1.6"**

>> *  In the venv, download and install:
>>> * asgiref                3.3.1
>>> * django-memcache-status 2.2
>>> * easy-thumbnails        2.7.1
>>> * fleep                  1.0.1
>>> * Pillow                 7.0.0
>>> * pip                    21.0.1
>>> * python-memcached       1.59
>>> * pytz                   2021.1
>>> * setuptools             41.6.0
>>> * six                    1.15.0
>>> * sqlparse               0.4.1
>>> * var-dump               1.2
>>> * wheel                  0.36.2

>> **django-admin startproject media**

>> **cd media**

>> **python manage.py migrate**

>> **python manage.py createsuperuser** 

>>> and follow the procedure on the screen

* The root folder of the project is 
 
    >**cacheandmediamanagementdjango** 
    
* and the absolute path is
 
    >**/var/www/html/cacheandmediamanagementdjango**
    
* Clone the project from gitlab on your Desktop :
    
    >**git@gitlab.com:greengo_axe/cacheandmediamanagementdjango.git**

* copy and overwrite all folders and files from 

>>**Desktop/cacheandmediamanagementdjango**

>into 

>>**/var/www/html/cacheandmediamanagementdjango**

* Delete the folder on your Desktop (**Desktop/cacheandmediamanagementdjango**)

## The start of a website

* Start memcached in a shell tab:

> **memcached -vv -l 127.0.0.1:11211**

* Start the Django's built-in server in another shell tab : 

> **/var/www/html/cacheandmediamanagementdjango/media> $ python manage.py runserver**

* Open your favourite browser and type this URL:

> **localhost:8000**

* Enjoy!

## F.A.Q

## Troubleshooting
  * This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.
  
## TODO List:
 * To improve CSS style
 * To improve validation
 * Whatever you like
